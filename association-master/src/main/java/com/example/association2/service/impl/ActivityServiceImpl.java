package com.example.association2.service.impl;

import com.example.association2.model.Activity;
import com.example.association2.model.ActivityDTO;
import com.example.association2.repository.ActivityRepository;
import com.example.association2.service.ActivityService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@CacheConfig(cacheNames = "activity")
public class ActivityServiceImpl implements ActivityService {
    @Autowired
    private ActivityRepository activityRepository;

    @Cacheable(value="findAct",key ="#p0")
    @Override
    public Activity findActivityById(int activityId) {
        return activityRepository.findActivityById(activityId);
    }

    @Cacheable(value="findActall",key ="'findAllAct'")
    @Override
    public List<Activity> findAllActivity() {
        return activityRepository.findAllActivity();
    }

    @Cacheable(value="findActall",key ="'findActByAssId_'+#p0")
    @Override
    public List<Activity> findActivityByAssociationId(int associationId) {
        return activityRepository.findActivityByAssociationId(associationId);
    }

    @CachePut(value="addAct",key = "'addAct_'+#p0")
    @Override
    public void addActivity(String title,
                            String description,
                            int associationId,
                            String startTime,
                            String endTime,
                            String location,
                            float material,
                            String picture) {
        activityRepository.addActivity(title,description,associationId,startTime,endTime,location,material,picture);
    }

    @CacheEvict(value = "findAct", key ="#p0",allEntries=true)
    @Override
    public void deleteActivity(int activityId) {
        activityRepository.deleteActivity(activityId);
    }

    @CachePut(value="updateAct",key = "'updateActStatus_'+#p0")
    @Override
    public void updateActivityStatus(int activityStatus, int activityId) {
        activityRepository.updateActivityStatus(activityStatus,activityId);
    }

    @CachePut(value="updateAct",key = "'updateAct_'+#p0")
    @Override
    public void updateActivity(String title, String description, int associationId, String startTime, String endTime, String location, String picture, float material, int activityStatus, int activityId) {
        activityRepository.updateActivity(title,description,associationId,startTime,endTime,location,picture,material,activityStatus,activityId);
    }

    @Cacheable(value="findAct",key ="'findActByAssType_'+#p0")
    @Override
    public List<Activity> findActivityByAssociationType(int associationType) {
        return activityRepository.findActivityByAssociationType(associationType);
    }

    @Cacheable(value="findAct",key ="'findActNameByAssType_'+#p0")
    @Override
    public List<ActivityDTO> findActivityAndNameByAssociationType(int associationType) {
        return activityRepository.findActivityAndNameByAssociationType(associationType);
    }
    @Cacheable(value="findActall",key ="'findAllActName'")
    @Override
    public List<ActivityDTO> findAllActivityAndName() {
        return activityRepository.findAllActivityAndName();
    }

    @Cacheable(value="findActall",key ="'findLastAct'")
    @Override
    public List<Activity> findLastActivity() {
        return activityRepository.findLastActivity();
    }

    @Cacheable(value="findActall",key ="'findAllActApp'")
    @Override
    public List<ActivityDTO> findAllActivityApply() {
        return activityRepository.findAllActivityApply();
    }

    @Override
    public void applyPass(int activityId) {
        activityRepository.applyPass(activityId);
    }

    @Override
    public void applyRefuse(int activityId) {
        activityRepository.applyRefuse(activityId);
    }

    @Cacheable(value="findActall",key ="'findActNameById_'+#p0")
    @Override
    public ActivityDTO findActivityAndNameById(int activityId) {
        return activityRepository.findActivityAndNameById(activityId);
    }
}
