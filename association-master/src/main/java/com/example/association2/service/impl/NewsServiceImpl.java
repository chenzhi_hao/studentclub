package com.example.association2.service.impl;

import com.example.association2.model.News;
import com.example.association2.repository.NewsRepository;
import com.example.association2.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@CacheConfig(cacheNames = "news")
@Service
public class NewsServiceImpl implements NewsService {
    @Autowired
    private NewsRepository newsRepository;

    @Cacheable(value="findNews",key ="#p0")
    @Override
    public News findNewsById(int id) {
        return newsRepository.findNewsById(id);
    }

    @Cacheable(value="findAllNews",key ="'findAllNews'")
    @Override
    public List<News> findAllNews() {
        return newsRepository.findAllNews();
    }

    @Cacheable(value="findAllNews",key ="'findLastNews'")
    @Override
    public List<News> findLastNews() {
        return newsRepository.findLastNews();
    }

    @CachePut(value="addNews",key = "'addNews_'+#p0")
    @Override
    public void addNews(String title, String description, String createTime) {
        newsRepository.addNews(title, description, createTime);
    }

    @CacheEvict(value = "findNews", key ="#p0",allEntries=true)
    @Override
    public void deleteNews(int id) {
        newsRepository.deleteNews(id);
    }

    @CachePut(value="updateNews",key = "'updateNews_'+#p0")
    @Override
    public void updateNews(String title, String description, int id) {
        newsRepository.updateNews(title, description, id);
    }
}
