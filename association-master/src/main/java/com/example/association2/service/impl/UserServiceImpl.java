package com.example.association2.service.impl;

import com.example.association2.model.User;
import com.example.association2.repository.UserRepository;
import com.example.association2.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@CacheConfig(cacheNames = "user")
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Cacheable(value="findAllUsers",key ="'findAllUsers'")
    @Override
    public List<User> findAllUsers() {
        return userRepository.findAllUsers();
    }

    @Override
    public User validUser(int userId, String password) {
        return userRepository.validUser(userId,password);
    }

    @CachePut(value="addUser",key = "'addUser_'+#p0")
    @Override
    public void addUser(int userId, String password, int type) {
        userRepository.addUser(userId,password,type);
    }

    @CacheEvict(value = "findUser1", key ="#p0",allEntries=true)
    @Override
    public void deleteUser(int userId) {
        userRepository.deleteUser(userId);
    }

    @CachePut(value="updateUser",key = "'updateUserPassword_'+#p0")
    @Override
    public void updateUserPassword(int userId, String password) {
     userRepository.updateUserPassword(userId,password);
    }

    @CachePut(value="updateUser",key = "'updateUserType_'+#p0")
    @Override
    public void updateUserType(int userId, int type) {
        userRepository.updateUserType(userId,type);
    }

    @Override
    public User validUserAccount(int userId) {
        return userRepository.validUserAccount(userId);
    }

    @Cacheable(value="findUser",key ="'findUserTypeById_'+#p0")
    @Override
    public int finUserTypeById(int userId) {
        return userRepository.finUserTypeById(userId);
    }

    @Cacheable(value="findUser1",key ="#p0")
    @Override
    public User finUserById(int userId) {
        return userRepository.finUserById(userId);
    }
}
