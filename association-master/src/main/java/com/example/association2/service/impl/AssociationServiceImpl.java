package com.example.association2.service.impl;

import com.example.association2.model.Association;
import com.example.association2.repository.AssociationRepository;
import com.example.association2.service.AssociationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@CacheConfig(cacheNames = "associationser")
@Service
public class AssociationServiceImpl implements AssociationService {
    @Autowired
    private AssociationRepository associationRepository;

    @Cacheable(value="findAssAlls",key ="'findAllAss'")
    @Override
    public List<Association> findAllAssociations() {
        return associationRepository.findAllAssociations();
    }

    @Cacheable(value="findAssAlls",key ="'findAssByType_'+#p0")
    @Override
    public List<Association> findAssociationsByType(int type) {
        return associationRepository.findAssociationsByType(type);
    }

    @Cacheable(value="findAsss",key ="'findAssById_'+#p0")
    @Override
    public Association findAssociationById(int associationId) {
        return associationRepository.findAssociationById(associationId);
    }

    @Cacheable(value="findAssAlls",key ="'findAssNameByAssId_'+#p0")
    @Override
    public List<String> findAssociationNameByAssociationId(int associationId) {
        return associationRepository.findAssociationNameByAssociationId(associationId);
    }

    @CachePut(value="addAss",key = "'addAss_'+#p0")
    @Override
    public void addAssociation(String description, String name, String createTime, int type, int ownerId,String logo) {
        associationRepository.addAssociation(description,name,createTime,type,ownerId,logo);
    }

    @Cacheable(value="findAssAlls",key ="'findAllAss1'")
    @Override
    public List<Association> findAllAss() {
        return associationRepository.findAllAss();
    }

    @CachePut(value="addAss",key = "'addAss_'+#p0")
    @Override
    public void addAss(String description, String name, String createTime, int type, int ownerId, String logo) {
        associationRepository.addAss(description,name,createTime,type,ownerId,logo);
    }

    @CacheEvict(value = "findAsss", key ="#p0",allEntries=true)
    @Override
    public void deleteAss(int associationId) {
        associationRepository.deleteAss(associationId);
    }

    @Cacheable(value="findAssAlls",key ="'findAllAssApply'")
    @Override
    public List<Association> findAllAssociationsApply() {
        return associationRepository.findAllAssociationsApply();
    }

    @Override
    public void applyPass(String createTime,int associationId) {
        associationRepository.applyPass(createTime,associationId);
    }

    @Override
    public void applyRefuse(int associationId) {
        associationRepository.applyRefuse(associationId);
    }

    @Override
    public void assModify(String description, String name, int type, int ownerId, String logo,Integer signatureActivity,int associationId) {
        associationRepository.assModify(description,name,type,ownerId,logo,signatureActivity,associationId);
    }

    @Cacheable(value="findAss",key ="'findSignatureActName_'+#p0")
    @Override
    public String findSignatureActivityName(int associationId) {
        return associationRepository.findSignatureActivityName(associationId);
    }

    @Cacheable(value="findAsss",key ="'findAssByUserId1_'+#p0")
    @Override
    public Association findAssByUserId(int userId) {
        return associationRepository.findAssByUserId(userId);
    }

    @Cacheable(value="findAsss",key ="'findOwnerIdByAssId_'+#p0")
    @Override
    public int findOwnerIdByAssociationId(int associationId) {
        return associationRepository.findOwnerIdByAssociationId(associationId);
    }

    @Override
    public int findAssociationIDByName(String name) {
        return associationRepository.findAssociationIDByName(name);
    }
}
