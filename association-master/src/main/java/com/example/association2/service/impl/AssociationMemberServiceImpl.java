package com.example.association2.service.impl;


import com.example.association2.model.Association;
import com.example.association2.model.AssociationMember;
import com.example.association2.model.AssociationMemberDTO;
import com.example.association2.repository.AssociationMemberRepository;
import com.example.association2.repository.AssociationRepository;
import com.example.association2.service.AssociationMemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@CacheConfig(cacheNames = "associationmem")
@Service
public class AssociationMemberServiceImpl implements AssociationMemberService {
    @Autowired
    private AssociationMemberRepository associationMemberRepository ;

    @Cacheable(value="findAssm",key ="#p0")
    @Override
    public int findassociationIdByUserId(int userId) {
        return associationMemberRepository.findassociationIdByUserId(userId);
    }

    @Cacheable(value="findPresidentm",key ="'findPreDesByAssid_'+#p0")
    @Override
    public String findPresidentDesByAssid(int associationId) {
        return associationMemberRepository.findPresidentDesByAssid(associationId);
    }

    @Cacheable(value="findAssAllm",key ="'findVicePreDesByAssid_'+#p0")
    @Override
    public List<String> findVicePresidentDesByAssid(int associationId) {
        return associationMemberRepository.findVicePresidentDesByAssid(associationId);
    }

    @Cacheable(value="findAssAllm",key ="'findMinisterDesByAssid_'+#p0")
    @Override
    public List<String> findMinisterDesByAssid(int associationId) {
        return associationMemberRepository.findMinisterDesByAssid(associationId);
    }

    @Cacheable(value="findAssAllm",key ="'findAllMem'")
    @Override
    public List<AssociationMemberDTO> findAllMember() {
        return associationMemberRepository.findAllMember();
    }

    @CacheEvict(value = "findAssm", key ="#p0",allEntries=true)
    @Override
    public void deleteUser(int Id) {
        associationMemberRepository.deleteUser(Id);
    }

    @Cacheable(value="findAssAllm",key ="'findMemberByUserId_'+#p0")
    @Override
    public List<AssociationMemberDTO> findMemberByUserId(int userId) {
        return associationMemberRepository.findMemberByUserId(userId);
    }

    @Cacheable(value="findMember",key ="'findMember_'+#p0")
    @Override
    public List<AssociationMemberDTO> findMember(int assid) {
        return associationMemberRepository.findMember(assid);
    }

    @CachePut(value="updateAssm",key = "'updateAm_'+#p0")
    @Override
    public void updateAm(int userId, String position, String description) {
        associationMemberRepository.updateAm(userId,position,description);
    }

    @CachePut(value="updateAssm",key = "'updatePreByAssId_'+#p0")
    @Override
    public void updatePresidentByAssociationId(int userId, int associationId) {
        associationMemberRepository.updatePresidentByAssociationId(userId,associationId);
    }

    @Cacheable(value="findAssAllm",key ="'findMemByUserId_'+#p0")
    @Override
    public AssociationMemberDTO findMemberById(int id) {
        return associationMemberRepository.findMemberById(id);
    }


    @Override
    public void addAssMem(int userid, int associationid, int position, String description) {
        associationMemberRepository.addAssMem(userid,associationid,position,description);
    }

    @Override
    public List<AssociationMember> selectMemberapply(int associationId) {
        return associationMemberRepository.selectAssMemapply(associationId);
    }

    @Override
    public AssociationMember selectAssMemapplybyUserId(int userid) {

        return associationMemberRepository.selectAssMemapplybyUserId(userid);
    }

    @Override
    public void addAssMemapply(int userid, int associationid, int position, String description) {
        associationMemberRepository.addAssMemapply(userid,associationid,position,description);
    }

    @Override
    public void deleteMemberapply(int userid) {
        associationMemberRepository.delAssMemapply(userid);
    }
}
