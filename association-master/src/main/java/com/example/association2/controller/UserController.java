package com.example.association2.controller;

import com.example.association2.model.*;
import com.example.association2.service.impl.ActivityServiceImpl;
import com.example.association2.service.impl.AssociationMemberServiceImpl;
import com.example.association2.service.impl.AssociationServiceImpl;
import com.example.association2.service.impl.UserServiceImpl;
import com.example.association2.utils.PicUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Controller
public class UserController {
    @Autowired
    private UserServiceImpl userService;
    @Autowired
    private AssociationMemberServiceImpl associationMemberService;
    @Autowired
    private AssociationServiceImpl associationService;
    @Autowired
    private ActivityServiceImpl activityService;

    @GetMapping("/userManage")
    public String toUserManage(@RequestParam(name = "bar", defaultValue = "1") int bar,
                               HttpServletRequest httpServletRequest) {
        switch (bar) {
            case 1:
                httpServletRequest.setAttribute("userList", userService.findAllUsers());
                httpServletRequest.setAttribute("bar", 1);
                break;
            case 2:
                httpServletRequest.setAttribute("AssociationMemberList", associationMemberService.findAllMember());
                httpServletRequest.setAttribute("bar", 2);
                break;
            case 3:
                httpServletRequest.setAttribute("bar", 3);
                break;
        }
        return "jsp/manage/user_manage";
    }

    @RequestMapping("/amDelete")
    public ModelAndView amDelete(@RequestParam(name = "id", required = true) int id,
                                 HttpServletRequest httpServletRequest) {
        associationMemberService.deleteUser(id);
        int bar = 2;
        ModelAndView mv = new ModelAndView();
        mv.addObject("bar", bar);
        mv.setViewName("forward:userManage");
        return mv;

    }
    @RequestMapping("/userDelete")
    public ModelAndView userDelete(@RequestParam(name = "userId", required = true) int userId) {
        userService.deleteUser(userId);
        ModelAndView mv = new ModelAndView();
        mv.setViewName("forward:userManage");
        return mv;
    }
    @GetMapping("/toUserModify")
    public String toModifyUser(@RequestParam(name = "userId", required = true) int userId,
                               HttpServletRequest httpServletRequest) {
        httpServletRequest.setAttribute("userId", userId);
        User user = userService.finUserById(userId);
        httpServletRequest.setAttribute("user", user);
        return "jsp/manage/user_modify";
    }
    @RequestMapping("/userModify")
    public ModelAndView userModify(@RequestParam(name = "userId", required = true) int userId,
                                   @RequestParam(name = "password", required = true) String password) {
        userService.updateUserPassword(userId, password);
        ModelAndView mv = new ModelAndView();
        mv.setViewName("forward:userManage");
        return mv;
    }
    @RequestMapping("/toAmModify")
    public String toModifyAm(@RequestParam(name = "id", required = true) int id,
                             HttpServletRequest httpServletRequest) {
        httpServletRequest.setAttribute("am", associationMemberService.findMemberById(id));
        return "jsp/manage/am_modify";
    }

    @RequestMapping("/amModify")
    public ModelAndView userModify(@RequestParam(name = "userId", required = true) int userId,
                                   @RequestParam(name = "position", required = true) String position,
                                   @RequestParam(name = "description", required = true) String description) {
        associationMemberService.updateAm(userId, position, description);
        ModelAndView mv = new ModelAndView();
        mv.setViewName("forward:userManage");
        return mv;
    }

    @RequestMapping("/profile")
    public String profile(HttpSession httpSession,
                          @RequestParam(name = "bar", defaultValue = "1") int bar,
                          HttpServletRequest httpServletRequest) {
        //返回用户
        httpServletRequest.setAttribute("user", userService.finUserById((int) httpSession.getAttribute("userId")));
        //查找社团社长
        List<AssociationMemberDTO> amd = associationMemberService.findMemberByUserId((int) httpSession.getAttribute("userId"));
        List<Association> associations = new ArrayList<>();
        Iterator<AssociationMemberDTO> iterator = amd.iterator();
        //返回创建的社团
        Association association = associationService.findAssByUserId((int) httpSession.getAttribute("userId"));
        switch (bar) {
            case 1:
                //遍历，添加到assjoin
                while (iterator.hasNext()) {
                    AssociationMemberDTO associationMemberDTO = iterator.next();
                    associations.add(associationService.findAssociationById(associationMemberDTO.getAssociationId()));
                }
                //返回加入的社团
                httpServletRequest.setAttribute("assJoin", associations);
                httpServletRequest.setAttribute("bar", 1);
                break;
            case 2:
                while (iterator.hasNext()) {
                    AssociationMemberDTO associationMemberDTO = iterator.next();
                    associations.add(associationService.findAssociationById(associationMemberDTO.getAssociationId()));
                }
                //返回创建的社团
                httpServletRequest.setAttribute("assCreate", associations);
                //返回申请的活动
                List<Activity> activities = activityService.findActivityByAssociationId(association.getAssociationId());
                if (activities != null) {
                    httpServletRequest.setAttribute("actApply", activities);
                    httpServletRequest.setAttribute("bar", 2);
                    break;
                } else {
                    httpServletRequest.setAttribute("bar", 2);
                    break;
                }

        }
        return "jsp/profile";
    }
    @RequestMapping("/toEditPassword")
    public String toEditPassword(HttpSession httpSession,HttpServletRequest httpServletRequest){
        httpServletRequest.setAttribute("userId",httpSession.getAttribute("userId"));
        return "jsp/edit_password";
    }
    @RequestMapping("/editPassword")
    public String editPassword(@RequestParam(name = "userId",required = true) int userId,
                               @RequestParam(name = "password", required = true) String password){
        userService.updateUserPassword(userId,password);
        return "jsp/password_edit_success";
    }
    //社团成员查询
    @RequestMapping("/userselect")
    public String userselect(HttpSession httpSession,
            HttpServletRequest httpServletRequest) {
        Association association = associationService.findAssByUserId((int) httpSession.getAttribute("userId"));
        httpServletRequest.setAttribute("AssociationMemberList",associationMemberService.findMember(association.getAssociationId()));
        return "jsp/user_select";
    }

    //申请加入社团
    @GetMapping("/userApply")
    public String touserApply(HttpServletRequest httpServletRequest) {
        HttpSession session = httpServletRequest.getSession();
        int userId = (int)session.getAttribute("userId");
        httpServletRequest.setAttribute("userId", userId);
        return "jsp/user_apply";
    }

    //提交加入社团申请
    @PostMapping("/userApply")
    @ResponseBody
    public String userApply(String name,
                         String description,
                         int userid1,
                         int position) {
        associationMemberService.addAssMemapply(userid1,associationService.findAssociationIDByName(name),position,description);
        String result = "1";
        return result;
    }

    //审核新成员
    @GetMapping("/SecondUserManage")
    public String SecondUserManage(@RequestParam(name = "bar", defaultValue = "1") int bar,
                                   HttpSession httpSession,
                               HttpServletRequest httpServletRequest) {
        Association association = associationService.findAssByUserId((int) httpSession.getAttribute("userId"));
        httpServletRequest.setAttribute("AssociationMemberList", associationMemberService.selectMemberapply(association.getAssociationId()));
        return "jsp/manage/user_Secondmanage";
    }
    //通过新成员
    @RequestMapping("/userApplyPass")
    public ModelAndView assApplySuccess(
            @RequestParam(name = "userid1", required = true) int userid1) {

        AssociationMember assMem = associationMemberService.selectAssMemapplybyUserId(userid1);
        associationMemberService.addAssMem(assMem.getUserId(),assMem.getAssociationId(),assMem.getPosition(),assMem.getDescription());
        associationMemberService.deleteMemberapply(userid1);

        ModelAndView mv = new ModelAndView();
        mv.setViewName("forward:SecondUserManage");
        return mv;
    }
    //拒绝新成员
    @RequestMapping("/userApplyRefuse")
    public ModelAndView assApplyRefuse(@RequestParam(name = "userid1", required = true) int userid1) {
        associationMemberService.deleteMemberapply(userid1);
        ModelAndView mv = new ModelAndView();
        mv.setViewName("forward:SecondUserManage");
        return mv;
    }

}