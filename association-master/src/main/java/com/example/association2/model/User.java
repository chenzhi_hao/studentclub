package com.example.association2.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class User  implements Serializable {
    private int userId;
    private String password;
    private int type;
}
