package com.example.association2.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class AssociationMember implements Serializable {
    private int id;
    private int userId;
    private int associationId;
    private int position;
    private String description;
}
