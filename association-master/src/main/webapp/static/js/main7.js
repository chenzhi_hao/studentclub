
/**
 * 点击上传按钮
 */
$('#zhl_admin_update_btn').on('click',function(){
    var name=document.getElementById("name").value;
    var description=document.getElementById("description").value;
    var userid1=document.getElementById("userid1").value;
    var position=document.getElementById("position ").value;

    var formData = new FormData();

    formData.append('name',name);
    formData.append('description',description);
    formData.append('userid1',userid1);
    formData.append('position',position);
    base64_uploading(formData)
})


/**
 * ajax上传base64自定义函数
 */
function base64_uploading(formData){
    // ajax 上传
    $.ajax({
        type : "POST",
        url : "/userApply",
        data : formData,
        processData: false,   // jQuery不要去处理发送的数据
        contentType: false,   // jQuery不要去设置Content-Type请求头
        dataType : "json",
        success : function(result) {
            location.href="/applyResult"
        }
    });
    // ajax 上传

}