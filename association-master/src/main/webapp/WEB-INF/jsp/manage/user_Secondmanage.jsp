<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.example.association2.model.User" %>
<%@ page import="com.example.association2.model.Association" %>
<%@ page import="java.util.List" %>
<%@ page import="static java.lang.Integer.parseInt" %>

<html lang="zh-CN">
<head>
    <%@include file="../head.jsp" %>
</head>
<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <!-- 引入bar -->
        <jsp:include page="../sidebar.jsp"/>
        <jsp:include page="../topbar.jsp"/>
        <!-- page content -->
        <div class="right_col" role="main">
            <div class="page-title">
                <div class="title_left">
                    <h3>用户管理</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class=" x_panel">
                        <div class="x_content">
                            <div class="row ">
                                <div class="col-md-12 col-sm-12 col-xs-12 text-center"><!--分类栏-->
                                    <!--主要内容-->
                                            <div class="x_content">
                                                <table class="table table-striped projects">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 5%">用户Id</th>
                                                        <th style="width: 5%">希望职位</th>
                                                        <th >描述</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <c:forEach var="am" items="${AssociationMemberList}">
                                                        <tr >
                                                            <td >${am.userId}</td>
                                                            <c:choose>
                                                                <c:when test="${am.position==1}">
                                                                    <td >部长</td>
                                                                </c:when>
                                                                <c:when test="${am.position==2}">
                                                                    <td >副部长</td>
                                                                </c:when>
                                                                <c:when test="${am.position==3}">
                                                                    <td >会员</td>
                                                                </c:when>
                                                            </c:choose>
                                                            <td >${am.description}</td>
                                                            <td>
                                                                <a href="/userApplyPass?userid1=${am.userId}" class="btn btn-sm btn-primary">同意</a>
                                                                <a href="/userApplyRefuse?userid1=${am.userId}" class="btn btn-sm btn-danger deleteBtn">拒绝</a>
                                                            </td>
                                                        </tr>
                                                    </c:forEach>
                                                    </tbody>
                                                </table>
                                            </div>
                                    <!--主要内容/-->
                                </div><!--分类栏/-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<jsp:include page="../footer.jsp"/>
</body>
</html>
